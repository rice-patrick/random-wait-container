FROM centos

RUN useradd -ms /bin/bash sleepuser
COPY sleep_random.sh /home/sleepuser/sleep_random.sh
RUN chown sleepuser /home/sleepuser/sleep_random.sh && \
    chmod 0755 /home/sleepuser/sleep_random.sh

WORKDIR /home/sleepuser
USER sleepuser

ENTRYPOINT ./sleep_random.sh
